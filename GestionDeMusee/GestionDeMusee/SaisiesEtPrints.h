
#pragma region Affichages

void afficherMenuPrincipale(int* choix);
void afficherSousMenu(int choixMenuSecondaire);
void afficherMenuGestionMusees();
void afficherMenuRechercheMusees();
void afficherMenuSauvegarde();
void afficherMenuErreur(int* choix);

#pragma endregion

#pragma region Saisies

float saisieFloat(float val);
int saisieInteger(int val);
void saisieChaine(char chaine);

#pragma endregion