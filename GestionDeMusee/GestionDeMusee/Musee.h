#ifndef DEF_MUSEE
#define DEF_MUSEE

#pragma region TypeDef

typedef struct Date Date;
typedef struct Musee Musee;
typedef struct Localisation Localisation;

#pragma endregion

#pragma region Structures

struct Date
{
	int jour;
	int mois;
	int annee;
};

struct Localisation
{
	char *Adresse[255];
	char *Ville[255];
	char *CodePostal[255];
	char *Departement[255];
	char *Region[255];
};

struct Musee
{
	char *Nom[150];
	char *SiteWeb[100];
	int Ferme;
	Localisation Localisation;
	int *AnneeReouverture;
	Date FermetureAnuelle;
	Date PeriodeOuverture;
};

#pragma endregion

void ModifierMusee(Musee *pointeurSurMusee, char *nom, char *prenom, char *dateNaissance);
void CreerDate(Date* date, char *strDate);
char *substr(char *src, int pos, int len);

#endif
