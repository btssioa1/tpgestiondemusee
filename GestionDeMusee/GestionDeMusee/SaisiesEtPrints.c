#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include"Main.h"

#define TAILLE_TABLEAU 255

#pragma region Affichages

//========================== AFFICHES ==========================//

void afficherSousMenu(int choixMenuSecondaire)
{
	do
	{
		switch (choixMenuSecondaire)
		{
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		default:
			break;
		}
	} while (choixMenuSecondaire == NULL);
	choixMenuSecondaire = NULL;

}

void afficherMenuPrincipale(int* choix)
{
	system("cls");
	printf("::---------------------------::\n");
	printf("::    Gestion des Musees     ::\n");
	printf("::                           ::\n");
	printf("::---------------------------::\n");
	printf("::  1 - Consulter            ::\n");
	printf("::  2 - Rechercher           ::\n");
	printf("::  3 - Sauvegarder          ::\n");
	printf("::---------------------------::\n");
	printf("::  0 - Quitter              ::\n");
	printf("::---------------------------::\n");
	printf("::                             \n");
	printf("::  Votre choix : ");
}

void afficherMenuGestionMusees()
{
	system("cls");
	printf("::---------------------------::\n");
	printf("::    Gestion des Musees     ::\n");
	printf("::       Consultation        ::\n");
	printf("::---------------------------::\n");
	printf("::  1 - Ajouter              ::\n");
	printf("::  2 - Modifier             ::\n");
	printf("::  3 - Supprimer            ::\n");
	printf("::---------------------------::\n");
	printf("::  0 - Menu principal       ::\n");
	printf("::---------------------------::\n");
	printf("::                             \n");
	printf("::  Votre choix : ");
}

void afficherMenuRechercheMusees()
{
	system("cls");
	printf("::---------------------------::\n");
	printf("::    Gestion des Musees     ::\n");
	printf("::        Recherche          ::\n");
	printf("::---------------------------::\n");
	printf("::                           ::\n");
	printf("::  1 - Rechercher           ::\n");
	printf("::                           ::\n");
	printf("::---------------------------::\n");
	printf("::  0 - Menu principal       ::\n");
	printf("::---------------------------::\n");
	printf("::                             \n");
	printf("::  Votre choix : ");
}

void afficherMenuSauvegarde()
{
	system("cls");
	printf("::---------------------------::\n");
	printf("::    Gestion des Musees     ::\n");
	printf("::        Sauvegarde         ::\n");
	printf("::---------------------------::\n");
	printf("::                           ::\n");
	printf("::  1 - Sauvegarder          ::\n");
	printf("::                           ::\n");
	printf("::---------------------------::\n");
	printf("::  0 - Menu principal       ::\n");
	printf("::---------------------------::\n");
	printf("::                             \n");
	printf("::  Votre choix : ");
}

void afficherMenuErreur(int* choix)
{
	system("cls");
	printf("::---------------------------::\n");
	printf("::    Gestion des Musees     ::\n");
	printf("::          Erreur           ::\n");
	printf("::---------------------------::\n");
	printf("::          Veuillez         ::\n");
	printf("::           Saisir          ::\n");
	printf("::     un menu existant      ::\n");
	printf("::---------------------------::\n");
	printf("::  0 - Menu principal       ::\n");
	printf("::---------------------------::\n");
	printf("::                             \n");
	printf("::  Votre choix : ");
}

#pragma endregion

#pragma region Saisies

//========================== SAISIE ==========================//

float saisieFloat(float val)
{
	printf("\n Veuillez saisir une valeur : ");
	scanf("%f", &val);
	fflush(stdin);
	return val;
}

int saisieInteger(int val)
{
	scanf("%d", &val);
	fflush(stdin);
	return val;
}

void saisieChaine(char chaine)
{
	printf("\n Veuillez saisir une chaine de caract\x8Ares (%d caract\x8Ares maximum) : ", TAILLE_TABLEAU);
	scanf("%s", &chaine);
	fflush(stdin);
}

#pragma endregion